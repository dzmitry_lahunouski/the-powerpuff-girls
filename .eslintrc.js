module.exports = {
  extends: ["airbnb", "plugin:react/recommended"],
  rules: {
    "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx"] }],
    semi: ["off"],
    "react/destructuring-assignment": ["never"],
    "react/prop-types": ["never"],
    "jsx-a11y/click-events-have-key-events": ["never"]
  },
  globals: {
    document: false,
    it: false,
    fetch: false,
    window: false,
  }
};
