import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import { Layout } from 'antd';
import Episode from '../episode'
import Home from '../home'
import Header from '../header'

import Paths from '../../constants/routes.constants'

import './App.css';

function App() {
  return (
    <Router>
      <Layout className="layout">
        <Header />
        <Switch>
          <Route exact path={Paths.HOME} component={Home} />
          <Route path={`${Paths.EPISODE}/:id`} component={Episode} />
        </Switch>
        <Layout.Footer style={{ textAlign: 'center' }}>Random Design ©2019 Created by Dzmitry</Layout.Footer>
      </Layout>
    </Router>
  );
}

export default App;
