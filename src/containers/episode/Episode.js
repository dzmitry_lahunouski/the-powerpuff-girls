import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import {
  Row,
  Col,
  Card,
  Button,
  Spin,
} from 'antd';
import General from '../../components/general'
import MainWrapper from '../../components/mainWrapper'

import { goToNewRoute, fetchEpisode } from '../../store/actions'
import * as selectors from '../../store/reducers'

import Paths from '../../constants/routes.constants'

import defaultImg from './no-img.png'
import './Episode.css'

class Episode extends React.PureComponent {
  componentDidMount() {
    const { id } = this.props.match.params
    const [showId, seasonNumber, episodeNumber] = id.split('-')
    this.props.fetchData(showId, seasonNumber, episodeNumber);
  }

  renderLoading() {
    return (
      <MainWrapper match={this.props.match}>
        <div className="content-wrapper--loading flex centered">
          <Spin tip="Loading..." />
        </div>
      </MainWrapper>
    )
  }

  render() {
    const { match, episode, goTo } = this.props
    if (!this.props.episode.name) return this.renderLoading();
    return (
      <MainWrapper match={match}>
        <div className="content-wrapper">
          <Row>
            <Col span={18} lg={16} xs={24} sm={24}>
              <General
                title={episode.name}
                content={episode.summary.replace(/<[^>]*>?/gm, '')}
                logo={episode.image ? episode.image.medium || defaultImg : defaultImg}
                actionBtn={(
                  <Button type="primary">
                    <Link to={{ pathname: Paths.HOME }} onClick={goTo}>
                      Back
                    </Link>
                  </Button>
                )}
              />
            </Col>
            <Col span={6} lg={8} xs={24} sm={24}>
              <div className="card-wrapper">
                <Card title="Episode info">
                  <div>
                    <b>Show: </b>
                    <span>{episode.network}</span>
                  </div>
                  <div>
                    <b>Number: </b>
                    <span>{`Season${episode.season}, Episode ${episode.number}`}</span>
                  </div>
                  <div>
                    <b>Airdate: </b>
                    <span>{`${new Date(episode.airdate).toDateString()} at ${episode.airtime}`}</span>
                  </div>
                  <div>
                    <b>Runtime: </b>
                    <span>{`${episode.runtime} minutes`}</span>
                  </div>
                  <div>
                    <b>Rating: </b>
                    <span>{episode.rating || 'no rated'}</span>
                  </div>
                </Card>
              </div>
            </Col>
          </Row>
        </div>
      </MainWrapper>
    )
  }
}

const mapStateToProps = state => ({
  episode: selectors.getSelectedEpisode(state),
})

const mapDispatchToProps = dispatch => ({
  goTo: () => {
    dispatch(goToNewRoute(Paths.HOME))
  },
  fetchData: (showId, seasonNumber, episodeNumber) => {
    dispatch(fetchEpisode(showId, seasonNumber, episodeNumber))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Episode);
