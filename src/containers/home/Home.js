/* eslint-disable max-len */
import React from 'react';
import { connect } from 'react-redux'
import {
  Row,
  Col,
  Card,
  Spin,
} from 'antd';

import { fetchBaseData } from '../../store/actions'
import * as selectors from '../../store/reducers'

import General from '../../components/general'
import MainWrapper from '../../components/mainWrapper'
import List from '../list'

import './Home.css';

function serializeSchedule({ days = [], time = '' } = {}) {
  if (!time || !days.length) {
    return 'No schedule'
  }
  if (days.length === 1) {
    return `${days}s at ${time}`
  }
  return `every ${days.join(' ')} at ${time}`
}

class Home extends React.PureComponent {
  componentDidMount() {
    this.props.dispatch(fetchBaseData());
  }

  renderLoading() {
    return (
      <MainWrapper match={this.props.match}>
        <div className="content-wrapper--loading flex centered">
          <Spin tip="Loading..." />
        </div>
      </MainWrapper>
    )
  }

  render() {
    if (!this.props.baseData.title) return this.renderLoading();
    return (
      <MainWrapper match={this.props.match}>
        <div className="content-wrapper">
          <Row>
            <Col span={18} lg={16} xs={24} sm={24}>
              <General
                title={this.props.baseData.title}
                content={this.props.baseData.content.replace(/<[^>]*>?/gm, '')}
                logo={this.props.baseData.image}
              />
            </Col>
            <Col span={6} lg={8} xs={24} sm={24}>
              <div className="card-wrapper">
                <Card title="Show info" style={{ width: '100%' }}>
                  <div>
                    <b>Network: </b>
                    <span>{this.props.baseData.network}</span>
                  </div>
                  <div>
                    <b>Schedule: </b>
                    <span>{serializeSchedule(this.props.baseData.schedule)}</span>
                  </div>
                  <div>
                    <b>Status: </b>
                    <span>{this.props.baseData.status}</span>
                  </div>
                  <div>
                    <b>Show Type: </b>
                    <span>{this.props.baseData.type}</span>
                  </div>
                  <div>
                    <b>Genres: </b>
                    <span>{this.props.baseData.genres.join(' | ')}</span>
                  </div>
                  <div>
                    <b>Rating: </b>
                    <span>{this.props.baseData.rating}</span>
                  </div>
                </Card>
              </div>
            </Col>
          </Row>
          {this.props.baseData.seasons.map(({ id, number }) => (
            <Row key={id}>
              <section className="list-wrapper">
                <List
                  seasonId={id}
                  seasonNumber={number}
                  header={<b>{`Season ${number}`}</b>}
                  showId={this.props.baseData.id}
                />
              </section>
            </Row>
          ))}
        </div>
      </MainWrapper>
    )
  }
}

const mapStateToProps = state => ({
  baseData: selectors.selectBaseData(state),
})

export default connect(
  mapStateToProps,
)(Home);
