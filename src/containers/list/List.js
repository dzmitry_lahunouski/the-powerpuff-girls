import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  List,
  Typography,
  Spin,
} from 'antd';

import { fetchDataForSeason, goToNewRoute } from '../../store/actions'
import * as selectors from '../../store/reducers'

import Paths from '../../constants/routes.constants'

import './List.css';

const Item = props => (
  <List.Item>
    <Link
      to={{ pathname: `${Paths.EPISODE}/${props.showId}-${props.seasonNumber}-${props.number}` }}
      onClick={props.onItemClick}
      className="episode-link"
    >
      <div className="episode-item">
        <Typography.Text code>{props.number}</Typography.Text>
        &nbsp;
        {props.name}
      </div>
    </Link>
  </List.Item>
)

class EpisodeList extends React.PureComponent {
  componentDidMount() {
    this.props.getEpisodes();
  }

  render() {
    if (!this.props.episodes) {
      return (
        <div className="flex centered">
          <Spin tip="Loading..." />
        </div>
      )
    }
    return (
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          xl: 3,
        }}
        size="large"
        header={<div>{this.props.header}</div>}
        bordered
        dataSource={this.props.episodes}
        renderItem={episode => (
          <Item
            {...episode}
            key={episode.id}
            onItemClick={this.props.goToEpisode}
            showId={this.props.showId}
            seasonNumber={this.props.seasonNumber}
          />
        )}
      />
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  episodes: selectors.selectEpisodesForSeason(state, ownProps.seasonId),
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  goToEpisode: ({ key } = {}) => dispatch(goToNewRoute(key)),
  getEpisodes: () => dispatch(fetchDataForSeason(ownProps.seasonId)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EpisodeList);
