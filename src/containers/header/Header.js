import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'

import { Layout, Menu } from 'antd';
import { goToNewRoute } from '../../store/actions'
import * as selectors from '../../store/reducers'

import Paths from '../../constants/routes.constants'

class Header extends React.PureComponent {
  componentDidMount() {
    window.onpopstate = () => {
      const { hash } = window.location
      if (!hash) {
        return this.props.changeFocus({ key: Paths.HOME })
      }
      const key = Object.values(Paths).find(hash) || Paths.HOME
      return this.props.changeFocus({ key })
    }
  }

  render() {
    return (
      <Layout.Header>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          onClick={this.props.changeFocus}
          selectedKeys={[this.props.current]}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key={Paths.HOME}>
            <Link to={{ pathname: Paths.HOME }}>Home</Link>
          </Menu.Item>
          <Menu.Item key="disabled" disabled>
            In development
          </Menu.Item>
        </Menu>
      </Layout.Header>
    )
  }
}

const mapStateToProps = state => ({
  current: selectors.selectActiveMenuItem(state),
})

const mapDispatchToProps = dispatch => ({
  changeFocus: ({ key }) => {
    dispatch(goToNewRoute(key))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header)
