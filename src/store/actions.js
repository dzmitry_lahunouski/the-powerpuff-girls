import * as types from './actionTypes'
import MazeService from '../services/tvmaze'

export const menuItemClick = key => ({
  type: types.MENU_ITEM_CLICK,
  key,
})

export const fetchBaseData = showName => async (dispatch) => {
  try {
    const data = await MazeService.getMainData(showName)
    const serializedData = {
      id: data.id,
      title: data.name,
      genres: data.genres,
      premiered: data.premiered,
      schedule: data.schedule,
      rating: data.rating.average,
      image: data.image.medium,
      content: data.summary,
      network: data.network.name,
      status: data.status,
      type: data.type,
      // eslint-disable-next-line no-underscore-dangle
      seasons: data._embedded.seasons.map(season => ({
        id: season.id,
        number: season.number,
        startDate: season.premiereDate,
        endDate: season.endDate,
        content: season.summary,
      })),
    }
    dispatch({ type: types.BASE_DATA_FETCHED, data: serializedData })
  } catch (error) {
    console.log(error)
  }
}

export const fetchDataForSeason = seasonId => async (dispatch) => {
  try {
    const data = await MazeService.getEpisodesForSeason(seasonId)
    dispatch({ type: types.SEASON_DATA_FETCHED, payload: { data, seasonId } })
  } catch (error) {
    console.log(error)
  }
}

export const goToNewRoute = (key = null) => (dispatch) => {
  dispatch({
    type: types.NEW_ROUTE,
    key,
  })
}

export const fetchEpisode = (showId, season, episode) => async (dispatch) => {
  try {
    const data = await MazeService.getEpisodeByNumber(showId, season, episode)
    dispatch({ type: types.UPDATE_EPISODE, payload: data })
  } catch (error) {
    console.log(error)
  }
}
