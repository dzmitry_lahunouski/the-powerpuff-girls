import * as types from './actionTypes'
import Paths from '../constants/routes.constants'

function baseDataReducer(baseData = {}, action) {
  switch (action.type) {
    case types.BASE_DATA_FETCHED:
      return action.data
    default:
      return baseData
  }
}

function episodesReducer(baseData = {}, action) {
  switch (action.type) {
    case types.SEASON_DATA_FETCHED:
      return {
        ...baseData,
        [action.payload.seasonId]: action.payload.data,
      }
    default:
      return baseData
  }
}

function newRouteReducer(activeMenuItem = Paths.HOME, action) {
  switch (action.type) {
    case types.NEW_ROUTE:
      return action.key
    default:
      return activeMenuItem
  }
}

function setEpisodeReducer(currentEpisode = {}, action) {
  switch (action.type) {
    case types.UPDATE_EPISODE:
      return action.payload
    default:
      return currentEpisode
  }
}

export function selectBaseData(state) {
  return state.baseData
}

export function selectEpisodesForSeason(state, seasonId) {
  return state.episodes[seasonId]
}

export function selectActiveMenuItem(state) {
  return state.activeMenuItem
}

export function getSelectedEpisode(state) {
  return state.currentEpisode
}

export const reducers = {
  baseData: baseDataReducer,
  episodes: episodesReducer,
  activeMenuItem: newRouteReducer,
  currentEpisode: setEpisodeReducer,
}
