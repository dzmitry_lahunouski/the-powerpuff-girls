/* eslint-disable max-len */
import React from 'react';
import { Layout } from 'antd';
import Crumbs from '../crumbs'

import './MainWrapper.css';

// eslint-disable-next-line react/prop-types
const MainWrapper = ({ children, match }) => (
  <Layout.Content>
    <div className="container">
      <Crumbs match={match} />
      {children}
    </div>
  </Layout.Content>
)

export default MainWrapper;
