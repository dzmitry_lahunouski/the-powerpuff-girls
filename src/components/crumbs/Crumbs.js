import React from 'react';
import { Breadcrumb } from 'antd';

const getCrumbs = ({ url = '/' } = {}) => {
  if (url === '/') {
    return ['Home']
  }
  return ['Home', ...url.split('/')]
}

const Crumbs = ({ match }) => (
  <Breadcrumb style={{ margin: '16px 0' }}>
    {getCrumbs(match).map(crumbTitle => (
      <Breadcrumb.Item key={crumbTitle}>{crumbTitle}</Breadcrumb.Item>
    ))}
  </Breadcrumb>
)

export default Crumbs;
