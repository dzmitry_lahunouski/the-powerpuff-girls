/* eslint-disable max-len */
import React from 'react';

import './General.css';

const General = props => (
  <section className="flex general">
    <aside className="general-img">
      <figure>
        <img src={props.logo} alt="The Powerpuff Girls" />
      </figure>
    </aside>
    <article className="general-text-wrapper">
      <h2 className="general-text-title">{props.title}</h2>
      <p>
        {props.content}
      </p>
      <div className="general-action-wrapper">
        {props.actionBtn}
      </div>
    </article>
  </section>
)

export default General;
