// services are state-less
// they act as utility facades that abstract the details for complex operations
// normally, our interface to any sort of server API will be as a service

const TV_MAZE_ENDPOINT = 'http://api.tvmaze.com';

const requestConfig = {
  method: 'GET',
  headers: {
    Accept: 'application/json',
  },
}

const baseRequest = async (methodName, url) => {
  const response = await fetch(url, requestConfig);
  if (!response.ok) {
    throw new Error(`MazeService ${methodName} failed, HTTP status ${response.status}`);
  }
  return response.json()
}

const MazeService = {

  async getMainData(showName = 'the-powerpuff-girls') {
    const url = `${TV_MAZE_ENDPOINT}/singlesearch/shows?q=${showName}&embed=seasons`;
    return baseRequest('getMainData', url)
  },

  async getEpisodesForSeason(seasonId) {
    const url = `${TV_MAZE_ENDPOINT}/seasons/${seasonId}/episodes`;
    return baseRequest('getEpisodesForSeason', url)
  },

  async getEpisodeByNumber(showId, season, episode) {
    const url = `${TV_MAZE_ENDPOINT}/shows/${showId}/episodebynumber?season=${season}&number=${episode}`;
    return baseRequest('getEpisodeByNumber', url)
  },
}

export default MazeService
